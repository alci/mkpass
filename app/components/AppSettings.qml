/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPass.

  MkPass is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPass.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import QtQuick.LocalStorage 2.0

QtObject {

    property var db: null
    property string defaultAlgo: ""
    property string maxChars: ""

    function openDB() {
        if(db !== null) return;

        // db = LocalStorage.openDatabaseSync(identifier, version, description, estimated_size, callback(db))
        db = LocalStorage.openDatabaseSync("MkPass", "1.0", "Mecadu Password blender", 1000);

        try {
            db.transaction(function(tx){
                tx.executeSql('CREATE TABLE IF NOT EXISTS settings(key TEXT UNIQUE, value TEXT)');
                var table  = tx.executeSql("SELECT * FROM settings");
                // Seed the table with default values
                if (table.rows.length === 0) {
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["defaultAlgo", "pbkdf2"]);
                    tx.executeSql('INSERT INTO settings VALUES(?, ?)', ["maxChars", "999"]);
                };
            });
        } catch (err) {
            console.log("Error creating table in database: " + err);
        };
    }


    function saveSetting(key, value) {
        openDB();
        db.transaction( function(tx){
            tx.executeSql('INSERT OR REPLACE INTO settings VALUES(?, ?)', [key, value]);
        });
    }

    function getSetting(key) {
        openDB();
        var res = "";
        db.transaction(function(tx) {
            var rs = tx.executeSql('SELECT value FROM settings WHERE key=?;', [key]);
            if (rs.rows.length > 0) {
                res = rs.rows.item(0).value;
            }
        });
        return res;
    }

    function getDefaultAlgo() {
        if (defaultAlgo === "") {
            defaultAlgo = getSetting("defaultAlgo")
        }
        return defaultAlgo
    }

    function getMaxChars() {
        if (maxChars === "") {
            maxChars = getSetting("maxChars")
        }
        return maxChars
    }


    function setDefaultAlgo(val) {
        defaultAlgo = val
        saveSetting("defaultAlgo", val)
    }

    function setMaxChars(val) {
        maxChars = val
        saveSetting("maxChars", val)
    }


}
