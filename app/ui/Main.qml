import QtQuick 2.0
import Ubuntu.Components 1.2
import "../components/crypto.js" as CJS
import "../components"
import "../UCSComponents"

Page {

    property string derivatedPass
    property AppSettings settings: AppSettings{}

    title: i18n.tr("A password blender **")

    head.actions: [
                Action {
                    iconName: "settings"
                    text: i18n.tr("settings")
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("SettingsPage.qml"),{"appSettings": settings})
                    }
                }
            ]

    Flickable {

        anchors {
            margins: units.gu(2)
            fill: parent
        }

        Label {
            id: label
            text: i18n.tr("Password")
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: units.gu(1)
        }

        TextField {
            id: mainpass
            echoMode: TextInput.PasswordEchoOnEdit
            anchors.top: label.bottom
            width: parent.width
            placeholderText: i18n.tr("A password you can remember, specific to the service...")
            focus: false
            onLengthChanged: {
                derivatedTextField.text = ""
                derivatedPass = ""
            }
        }

        Image {
            id: blenderIcon
            source: "../graphics/mkpass.png"
            anchors.horizontalCenter: mainpass.horizontalCenter
            anchors.top: mainpass.bottom
            anchors.topMargin: units.gu(4)
            anchors.bottomMargin: units.gu(4)
            width: units.gu(10)
            height: units.gu(10)

            MouseArea {
                anchors.fill: parent
                onPressAndHold: {

                }

                onClicked: {
                    parent.focus = true
                    Haptics.play({duration: 10, attackIntensity: 0.9})
                    derivatedTextField.text = ""
                    derivatedPass = ""
                    if(settings.getDefaultAlgo() === "sha1") {
                        derivatedPass = sha1(mainpass.text)
                    }
                    else if(settings.getDefaultAlgo() === "pbkdf2") {
                        derivatedPass = pbkdf2(mainpass.text)
                    }
                    if(derivatedPass.length > settings.getMaxChars()) {
                        derivatedPass = derivatedPass.substring(0, settings.getMaxChars())
                    }

                    derivatedTextField.text = derivatedPass
                    Haptics.play({duration: 500, attackIntensity: 0.9})
                }
            }
        }


        Label {
            id: derivatedLabel
            text: i18n.tr("Derivated Password")
            anchors.top: blenderIcon.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottomMargin: units.gu(1)
            anchors.topMargin: units.gu(4)
        }

        TextField {
            id: derivatedTextField
            anchors.top: derivatedLabel.bottom
            width: parent.width
            focus: false

        }
    }

    RadialBottomEdge {
        id: clip
        visible: true
        actions: [
            RadialAction {
                id: copy
                iconSource: "../graphics/copy.png"
                onTriggered: {
                    Clipboard.push(derivatedPass)
                }
                text: i18n.tr("Copy to clipboard")
            },
            RadialAction {
                id: clear
                //enabled: Clipboard
                iconName: "clear"
                onTriggered: {
                    Clipboard.push("")
                    derivatedTextField.text = ""
                    derivatedPass = ""
                    mainpass.text = ""
                }
                text: i18n.tr("Clear")
            }
        ]
    }

    function sha1(input) {
        var hash = CJS.CryptoJS.SHA1(input)
        return hash.toString(CJS.CryptoJS.enc.Base64)
    }

    function pbkdf2(input) {
        var hash = CJS.CryptoJS.PBKDF2(input, '1', { keySize: 128/32, iterations: 1000 })
        return CJS.CryptoJS.enc.Base64.stringify(hash)
    }

}
