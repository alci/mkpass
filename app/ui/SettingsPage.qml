/*
  Copyright (C) 2013 Franck Routier, Mecadu, the doubting mechanism
  Contact: Franck Routier <alci@mecadu.org>
  All rights reserved.

  This file is part of MkPokerPlanning.

  MkPokerPlanning is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  MkPokerPlanning is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with MkPokerPlanning.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Ubuntu.Components 1.2
import Ubuntu.Components.ListItems 0.1
import "../components/"


Page {
    id: settingsPage

    property AppSettings appSettings

    title: i18n.tr("Settings")

    function getAlgoIndex() {
        if (appSettings.getDefaultAlgo() === "sha1")
            return 0
        else if (appSettings.getDefaultAlgo() === "pbkdf2")
            return 1
        else return 0
    }

    Flickable {
        anchors.fill: parent
        anchors.leftMargin: units.gu(2)
        anchors.rightMargin: units.gu(2)
        anchors.topMargin: units.gu(1)

        Column {
            anchors.fill: parent

            OptionSelector {
                id: defaultAlgo
                objectName: "defaultAlgoSelector"
                text: i18n.tr("Algorithm")
                model: [i18n.tr("sha1"),
                        i18n.tr("pbkdf2")]
                selectedIndex: getAlgoIndex()
                onSelectedIndexChanged: {
                    if(selectedIndex === 0)
                        appSettings.setDefaultAlgo("sha1")
                    if(selectedIndex === 1)
                        appSettings.setDefaultAlgo("pbkdf2")
                }
            }

            Label {
                font.italic: true
                fontSize: "small"
                wrapMode: "WordWrap"
                width: parent.width
                text: i18n.tr("Choose how to derivate a strong password from your clear text password. Can be either sha1 (quicker but deemed insecure) or PBKDF2 with a keysize of 128 bits and 1000 iterations. In both cases, the result is then base64 encoded.")
            }

            Text {
                text: i18n.tr("Max chars : ")+parseInt(maxCharsText.value)
            }

            Slider {
               id: maxCharsText
               width: parent.width
               minimumValue: 1.0
               maximumValue: 30.0
               value: appSettings.getMaxChars()
               live: false
               onValueChanged: {
                   appSettings.setMaxChars(parseInt(value))
                   parent.focus = true;
               }
            }

            Label {
                font.italic: true
                fontSize: "small"
                wrapMode: "WordWrap"
                width: parent.width
                text: i18n.tr("Truncate the derivated password for sites or apps that allow a maximum length for your password (which is quite stupid, but PayPal for example do!)\n\n")
            }

            Label {
                     width: parent.width
                     wrapMode: Text.WordWrap
                     horizontalAlignment: Text.Center
                     fontSize: "small"
                     text: i18n.tr("\n\nVersion 1.0")
                 }
             Label {
                 width: parent.width
                 wrapMode: Text.WordWrap
                 horizontalAlignment: Text.Center
                 fontSize: "small"
                 text: i18n.tr("\n\nDistributed under GPLv3 license.\nSource code available on Gitlab\nhttps://gitlab.com/mkpass\n©2015 mecadu, the doubting mechanism.")
             }


        }
    }
}
