��          �      <      �  �   �     5     C  7   Y  	   �  �   �     �     �     �     �     �     �  �   �     `     g     n     w  l  |  �   �     k     y  .   �  
   �  	  �     �     �     �          $     1  �   =     �     �     �     	                                                            
      	                                   

Distributed under GPLv3 license.
Source code available on Gitlab
https://gitlab.com/mkpass
©2015 mecadu, the doubting mechanism. 

Version 1.0 A password blender ** A password you can remember, specific to the service... Algorithm Choose how to derivate a strong password from your clear text password. Can be either sha1 (quicker but deemed insecure) or PBKDF2 with a keysize of 128 bits and 1000 iterations. In both cases, the result is then base64 encoded. Clear Copy to clipboard Derivated Password Max chars :  Password Settings Truncate the derivated password for sites or apps that allow a maximum length for your password (which is quite stupid, but PayPal for example do!)

 mkpass pbkdf2 settings sha1 Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-15 23:06+0200
PO-Revision-Date: 2015-07-15 23:07+0100
Last-Translator: Franck Routier <alcimecadu.org>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.7.5
 

Distribué sous licence GPLv3
Code source disponible sur Gitlab
http://gitlab.com.mkpass
©2015 mecadu, le mécanisme dubitatif 

Version 1.0 Un mixer à mots de passe ** Un mot de passe mémorisable et spécifique... Algorithme Choisissez comment dériver un mot de passe fort depuis votre mot de passe mémorisable. Vous pouvez utiliser sha1 (plus rapide mais moins sûr) ou PBKDF2 avec une taille de clé de 128 bits t 1000 itérations. Dans les deux cas, le résultat est encodé en base64. Effacer Copier dans le presse-papier Mot de passe dérivé Longueur max : Mot de passe Paramètres Tronquer le mot de passe dérivé pour les sites ou applications qui limitent la longueur de votre mot de passe (ce qui est stupide, mais PayPal par exemple le fait!)

 mkpass pbkdf2 préférences sha1 