��          �      <      �  �   �     5     C  7   Y  	   �  �   �     �     �     �     �     �     �  �   �     `     g     n     w  �  |  �        �     �  M   �        -       :     C     ^     u     �     �  r   �     	     	      	     .	                                                            
      	                                   

Distributed under GPLv3 license.
Source code available on Gitlab
https://gitlab.com/mkpass
©2015 mecadu, the doubting mechanism. 

Version 1.0 A password blender ** A password you can remember, specific to the service... Algorithm Choose how to derivate a strong password from your clear text password. Can be either sha1 (quicker but deemed insecure) or PBKDF2 with a keysize of 128 bits and 1000 iterations. In both cases, the result is then base64 encoded. Clear Copy to clipboard Derivated Password Max chars :  Password Settings Truncate the derivated password for sites or apps that allow a maximum length for your password (which is quite stupid, but PayPal for example do!)

 mkpass pbkdf2 settings sha1 Project-Id-Version: mkpass
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-16 08:05+0200
PO-Revision-Date: 2015-07-16 08:09+0100
Last-Translator: Franck Routier <alcimecadu.org>
Language-Team: German <de@li.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-07-16 05:56+0000
X-Generator: Poedit 1.7.5
 

Vertrieben unter der GPLv3 Lizenz.
Quellcode ist auf Gitlab verfügbar.
https://gitlab.com/mkpass
©2015 mecadu, the doubting mechanism. 

Version 1.0 Ein Passwort Mixer ** Ein Passwort das du dir, bezogen auf einen bestimmten Service, merken kannst. Algorithmus Wähle wie das starke Passwort aus deinem Klartextpasswort abgeleitet werden soll. Du kannst dich zwischen sha1(schneller aber gilt als unsicher) oder PBKDF2 mit einer Schlüsselgröße von 128 bit mit 1000 Verarbeitungsschritten entscheiden. In beiden fällen wird das Ergebnis base64 verschlüsselt. Löschen zu Zwischenablage kopieren Verarbeitetes Passwort Maximale Zeichen:  Passwort Einstellungen Kürzt das erstellte Passwort für Webseiten oder Apps die nur bestimmte Passwortlängen erlauben (z.B. PayPal).

 mkpass pbkdf2 Einstellungen sha1 